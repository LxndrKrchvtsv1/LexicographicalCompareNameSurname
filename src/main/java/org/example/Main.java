package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] names = new String[]{"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] surname = new String[]{"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};

        for (int i = 1; i < names.length; i++) {
            for (int j = 0; j < names.length - i; j++) {
                if (compareStrings(names[j], names[j + 1]) > 0) {
                    String tempName = names[j + 1];
                    String tempSurname = surname[j + 1];

                    surname[j + 1] = surname[j];
                    surname[j] = tempSurname;

                    names[j + 1] = names[j];
                    names[j] = tempName;
                }
            }

        }

        display(names);
        display(surname);
    }

    public static int compareStrings(String str1, String str2) {
        return str1.compareTo(str2);
    }

    public static void display(String[] array) {
        System.out.print(Arrays.toString(array) + "\n");
    }
}